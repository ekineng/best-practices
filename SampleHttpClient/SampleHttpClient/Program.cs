﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SampleHttpClient
{
    class MainClass
    {
        public static async Task Main(string[] args)
        {
            Console.WriteLine("========= BAD PRACTICE ========= ");

            using (HttpClient httpClient = new HttpClient())
            {
                var result = await httpClient.GetAsync("https://google.com");

                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Console.WriteLine("Response: OK");
                }
                else
                {
                    Console.WriteLine("Response: Error");
                }
            }

            Console.WriteLine("========= DONE =========");
        }

        public static async Task Main(string[] args)
        {
            Console.WriteLine("========= BAD PRACTICE ========= ");

            for (int i = 0; i < 100; i++)
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    var result = await httpClient.GetAsync("https://google.com");

                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Console.WriteLine("Response: OK");
                    }
                    else
                    {
                        Console.WriteLine("Response: Error");
                    }
                }
            }

            Console.WriteLine("========= DONE =========");

            // open terminal run netstat, then you should be able to see some WAIT ports waiting for FIN connection from the host server
        }

        //public static async Task Main(string[] args)
        //{
        //    Console.WriteLine("========= BEST Practice =========");
        //    HttpClient httpClient = new HttpClient();

        //    for (int i = 0; i < 100; i++)
        //    {
        //        var result = await httpClient.GetAsync("https://google.com");

        //        if (result.StatusCode == System.Net.HttpStatusCode.OK)
        //        {
        //            Console.WriteLine("Response: OK");
        //        }
        //        else
        //        {
        //            Console.WriteLine("Response: Error");
        //        }
        //    }

        //    Console.WriteLine("========= DONE =========");
        //}
    }
}
