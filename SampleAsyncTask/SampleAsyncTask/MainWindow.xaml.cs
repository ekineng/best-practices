﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SampleAsyncTask
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HttpClient _httpClient = new HttpClient();

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await CallMethod1();
            btn1.Background = Brushes.Red;
        }

        //private async void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    Task task = CallMethod1();
        //    task.Wait(); // deadlock
        //    btn1.Background = Brushes.Red;
        //}

        private async Task CallMethod1()
        {
            for (int i = 0; i < 5; i++)
            {
                Debug.WriteLine("Before call http async");

                HttpResponseMessage response = await _httpClient.GetAsync("https://google.com");
                string retVal = await response.Content.ReadAsStringAsync();

                Debug.WriteLine("After call http async");
            }
        }

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        CallMethod2();
        //        btn1.Background = Brushes.Red;
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        private async void CallMethod2()
        {
            int i = 1;
            int j = 0;
            int k = i / j;

            await Task.Delay(1000);
        }

        //private async void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    await CallMethod3()l
        //    btn1.Background = Brushes.Red;
        //}

        private async Task CallMethod3()
        {
            for (int i = 0; i < 10; i++)
            {
                HttpResponseMessage response = await _httpClient.GetAsync("https://google.com");
                string retVal = await response.Content.ReadAsStringAsync();
            }
        }
    }
}
